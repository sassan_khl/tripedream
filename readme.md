#TripeDream est un site de voyage crée avec le framework symfony 5

Lien mise en production : https://mighty-harbor-84070.herokuapp.com/

#Environnement de développement

- Symfony 5.3.9
- PHP 7.24
- Composer
- Docker
- Docker-compose

#Lancer le projet

docker-compose up-d
docker exec -ti id-fpm bash
symfony server:start / serve -d
Puis se rendre sur http://localhost:5000/

